package problemloader

import (
	"database/sql"
	"testing"

	"github.com/stretchr/testify/assert"

	_ "github.com/mattn/go-sqlite3"
)

func TestSQLProblemLoaderSQLite(t *testing.T) {
	db, err := sql.Open("sqlite3", "../testdata/sqlite.db")
	assert.NoError(t, err)

	var loader = NewSQLLoader(db)
	problem, err := loader.Load()
	assert.NoError(t, err)

	var resources = problem.Resources()
	assert.EqualValues(t, 4, resources.Len())
	var graph = problem.Graph()
	assert.EqualValues(t, 6, len(graph.TasksIDs()))

	// Resource loaded test
	var res = resources.Get(3)
	assert.NotNil(t, res)
	assert.EqualValues(t, "Пластик", res.Name())
	assert.EqualValues(t, 15, res.Amount(0))
	assert.EqualValues(t, 30, res.Amount(20))

	// Task load testing
	task := graph.Task(2)
	assert.NoError(t, err)
	assert.EqualValues(t, "Производство 500 деталей РТ3", task.Name())
	assert.EqualValues(t, 30, task.Effort())

}
