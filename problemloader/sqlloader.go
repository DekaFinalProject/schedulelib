package problemloader

import (
	"database/sql"

	"gitlab.com/DekaFinalProject/schedulelib/schedule"
	"gitlab.com/DekaFinalProject/schedulelib/schedule/simple"
)

type sqlLoader struct {
	db *sql.DB

	converter *Converter
	factory   schedule.Factory
}

func NewSQLLoader(db *sql.DB) Loader {
	return &sqlLoader{
		db:        db,
		converter: NewConverter(),
		factory:   simple.NewFactory(),
	}
}

func (l *sqlLoader) Load() (schedule.Problem, error) {
	err := l.loadResources()
	if err != nil {
		return nil, err
	}
	err = l.loadTasks()
	if err != nil {
		return nil, err
	}

	return l.converter.Problem()
}

func (l *sqlLoader) loadResources() error {
	resourcesRows, err := l.db.Query(`SELECT ID, Name, Consumable FROM Resource;`)
	if err != nil {
		return err
	}

	defer resourcesRows.Close()

	for resourcesRows.Next() {
		var (
			id         int64
			name       string
			consumable bool
		)

		resourcesRows.Scan(&id, &name, &consumable)

		var resource = l.factory.NewResource(schedule.Identifier(id))
		resource.SetName(name)
		resource.SetConsumable(consumable)
		timepointsRows, err := l.db.Query(`SELECT TimePoint, Amount FROM ResourceDiagramPoint WHERE ResourceID = ? ORDER BY TimePoint ASC;`, id)
		if err != nil {
			return err
		}
		defer timepointsRows.Close()

		var previousAmount schedule.ResourceAmount = 0
		for timepointsRows.Next() {
			var (
				time   uint
				amount schedule.ResourceAmount
			)
			timepointsRows.Scan(&time, &amount)

			var delta = amount - previousAmount
			// A zero result will cause no update
			if 0 < delta {
				resource.Add(delta, schedule.Time(time))
			} else if delta < 0 {
				resource.Subtract(-delta, schedule.Time(time))
			}
			previousAmount = amount
		}

		l.converter.AddResource(resource)
	}

	return nil
}

func (l *sqlLoader) loadTasks() error {
	// --- Loading tasks ---
	taskRows, err := l.db.Query(`SELECT ID, Name, Effort FROM Task;`)
	if err != nil {
		return err
	}
	defer taskRows.Close()

	for taskRows.Next() {
		var (
			id     int64
			name   string
			effort uint
		)

		taskRows.Scan(&id, &name, &effort)
		var t = NewTask(schedule.Identifier(id))
		t.SetName(name)
		t.SetEffort(schedule.Time(effort))

		// - Load resource requirements -
		resourceRequirementRows, err := l.db.Query(`SELECT ResourceID, Amount FROM ResourceRequirement WHERE TaskID = ? ;`, id)
		if err != nil {
			return err
		}
		defer resourceRequirementRows.Close()

		for resourceRequirementRows.Next() {
			var (
				rid    int64
				amount float64
			)
			resourceRequirementRows.Scan(&rid, &amount)
			t.SetResourceRequirement(schedule.Identifier(rid), schedule.ResourceAmount(amount))
		}

		// - Query prev tasks -
		prevTaskIDsRows, err := l.db.Query(`SELECT PreviousTaskID FROM PreviousTask WHERE TaskID = ? ;`, id)
		if err != nil {
			return err
		}
		defer prevTaskIDsRows.Close()
		for prevTaskIDsRows.Next() {
			var id int64
			prevTaskIDsRows.Scan(&id)
			t.AddPreviousTask(schedule.Identifier(id))
		}

		// Add task
		l.converter.AddTask(t)
	}

	return nil
}
