package problemloader

import (
	"database/sql"

	"gitlab.com/DekaFinalProject/schedulelib/schedule"
)

type Loader interface {
	Load() (schedule.Problem, error)
}

type factoryFuncType func(interface{}) Loader
type Factory struct {
	loaders map[string]factoryFuncType
}

func NewFactory() *Factory {
	var out Factory
	out.loaders = make(map[string]factoryFuncType)

	out.loaders["sql"] = func(i interface{}) Loader {
		db, ok := i.(*sql.DB)
		if !ok {
			return nil
		}
		return NewSQLLoader(db)
	}

	return &out
}

func (f *Factory) NewLoader(loaderType string, config interface{}) Loader {
	createFunc, ok := f.loaders[loaderType]
	if !ok {
		return nil
	}

	return createFunc(config)
}
