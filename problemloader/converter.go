package problemloader

import (
	"gitlab.com/DekaFinalProject/schedulelib/schedule"
	"gitlab.com/DekaFinalProject/schedulelib/schedule/simple"
)

type Converter struct {
	problem Problem

	factory schedule.Factory

	tasks map[schedule.Identifier]Task
}

func NewConverter() *Converter {
	var out Converter
	out.factory = simple.NewFactory()
	out.problem.ProblemGraph = out.factory.NewGraph()
	out.problem.ProblemResources = simple.NewMapResourceCollection()
	out.tasks = make(map[schedule.Identifier]Task)

	return &out
}

func (c *Converter) AddTask(task Task) {
	c.tasks[task.ID()] = task
}

func (c *Converter) AddResource(resource schedule.Resource) {
	c.problem.ProblemResources.Insert(resource)
}

func (c *Converter) Problem() (schedule.Problem, error) {
	c.problem.ProblemGraph = c.factory.NewGraph()
	if err := c.buildGraph(); err != nil {
		return nil, err
	}

	return c.problem, nil
}

func (c *Converter) buildGraph() error {
	// generate ids for event
	// mapping EventID -> list of task
	var eventIDMapping = make(map[schedule.Identifier][]schedule.Identifier)
	var currentID int64 = 1 // 0 reserved for start position
	// Start and end point of tasks Identifer -> EventID
	var startPoints = make(map[schedule.Identifier]schedule.Identifier)
	var endPoints = make(map[schedule.Identifier][]schedule.Identifier)

	for taskID, rawtask := range c.tasks {
		t, ok := rawtask.(*task)
		if !ok {
			panic("type Converter can work only with tasks from package problemloader")
		}

		var prevTasks = t.previousTask
		var found = false
		var eventID schedule.Identifier = 0
		// Check if we have already
		for eid, outPrevTasks := range eventIDMapping {
			if len(prevTasks) != len(outPrevTasks) {
				continue
			}

			var allequals = true
			for _, t1ID := range outPrevTasks {
				var idFound = false
				for _, t2ID := range prevTasks {
					if t1ID == t2ID {
						idFound = true
						break
					}
				}

				if !idFound {
					allequals = false
					break
				}
			}

			if allequals { // Set of task already exists
				found = true
				eventID = eid
				break
			}
		}

		if !found { // Add set
			if len(prevTasks) == 0 {
				eventIDMapping[0] = prevTasks
				eventID = 0
			} else {
				eventIDMapping[schedule.Identifier(currentID)] = prevTasks
				eventID = schedule.Identifier(currentID)
				currentID++
			}
		}

		// Assign start point of the task
		startPoints[taskID] = eventID

		// Assign end points if it is new point
		if !found {
			for _, prevID := range prevTasks {
				s, ok := endPoints[prevID]
				if !ok {
					s = make([]schedule.Identifier, 0)
				}

				endPoints[prevID] = append(s, eventID)
			}
		}
	}

	// Adding tasks
	for taskID, rawtask := range c.tasks {
		t, ok := rawtask.(*task)
		if !ok {
			panic("type Converter can work only with tasks from package problemloader")
		}

		var realTask = c.factory.NewTask(t.id)
		realTask.SetName(t.name)
		realTask.SetEffort(t.effort)
		for rID, rAmount := range t.resourceRequirements {
			realTask.SetResourceRequirement(rID, rAmount)
		}

		c.problem.ProblemGraph.AddTasks(realTask)

		startP, ok := startPoints[taskID]
		if !ok { // This is not normal. Start event must be exists for any task
			panic("Internal program error")
		}

		endP, ok := endPoints[taskID]
		if !ok { // It is end task. We should connect it to lastOne event with ID: currentID
			c.problem.ProblemGraph.AddTaskEdge(taskID, startP, schedule.Identifier(currentID))
			continue
		}

		for _, p := range endP {
			c.problem.ProblemGraph.AddTaskEdge(taskID, startP, p)
		}

	}

	return nil
}
