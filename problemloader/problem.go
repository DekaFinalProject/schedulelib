package problemloader

import "gitlab.com/DekaFinalProject/schedulelib/schedule"

// Problem is a problem implementation for loader.
//
// The loaders should create it uninitalized
// and fill with some implementations of graph and resource collection
type Problem struct {
	ProblemGraph     schedule.AOEGraphModifiable
	ProblemResources schedule.ResourceCollection
}

func (p Problem) Graph() schedule.AOEGraph {
	return p.ProblemGraph
}

func (p Problem) Resources() schedule.ResourceCollection {
	return p.ProblemResources
}
