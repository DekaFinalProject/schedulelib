package problemloader

import "gitlab.com/DekaFinalProject/schedulelib/schedule"

type Task interface {
	// SetID sets id of the task
	SetID(id schedule.Identifier)
	// ID gets id of the task
	ID() schedule.Identifier

	// SetName sets name
	SetName(name string)
	// SetEffort sets effort
	SetEffort(time schedule.Time)

	// SetResourceRequirement sets resource requirement for the task
	SetResourceRequirement(id schedule.Identifier, amount schedule.ResourceAmount)

	// AddPreviousTask add task before the current task
	AddPreviousTask(id schedule.Identifier)
}

type task struct {
	id     schedule.Identifier
	name   string
	effort schedule.Time

	previousTask         []schedule.Identifier
	resourceRequirements map[schedule.Identifier]schedule.ResourceAmount
}

func NewTask(id schedule.Identifier) Task {
	return &task{
		id:     id,
		name:   "",
		effort: 0,

		previousTask:         make([]schedule.Identifier, 0),
		resourceRequirements: make(map[schedule.Identifier]schedule.ResourceAmount),
	}
}

func (t *task) SetID(id schedule.Identifier) {
	t.id = id
}

func (t task) ID() schedule.Identifier {
	return t.id
}

func (t *task) SetName(name string) {
	t.name = name
}

func (t *task) SetEffort(time schedule.Time) {
	t.effort = time
}

func (t *task) SetResourceRequirement(id schedule.Identifier, amount schedule.ResourceAmount) {
	t.resourceRequirements[id] = amount
}

func (t *task) AddPreviousTask(id schedule.Identifier) {
	t.previousTask = append(t.previousTask, id)
}
