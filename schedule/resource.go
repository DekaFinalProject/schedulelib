package schedule

type Resource interface {
	TimeDiagram
	Copyable

	// SetID sets id
	SetID(id Identifier)
	// ID returns id
	ID() Identifier

	// SetName sets name
	SetName(name string)
	// Name returns name
	Name() string

	// SetConsumable sets if resource is consumable
	SetConsumable(yes bool)
	// Consumable returns if resource is consumable
	Consumable() bool

	// Acquire tries to subtract required amount of resources
	// as fast as possible. Returns time when resources captured.
	// Error will be generated in case of resource will never be available .
	//
	// If resource is not consumable it can also release resource after specified duration
	Acquire(amount ResourceAmount, time Time, duration Time) (Time, error)
}

type ResourceCollection interface {
	Copyable

	// Insert inserts resource in collection.
	Insert(resource Resource)
	// Get returns resource by ID
	// If no items with ID, Get returns nil
	Get(id Identifier) Resource
	// ResourceIDs returns ids of items in collection
	ResourceIDs() []Identifier
	// Len returns amount of items in collection
	Len() int
}

type ResourceRequirements interface {
	// SetResourceRequirement sets amount of required resource
	SetResourceRequirement(id Identifier, amount ResourceAmount)
	// AcquireResources tries to satisfy resource requirements.
	//
	// If resource requirements are meet then all required Resource
	// from collection subtract number of required resource
	// and returns time when all resources can be acquired.
	// If it is not possible, error will be generated
	AcquireResources(collection ResourceCollection, time Time, duration Time) (Time, error)
}
