package simple

import (
	"fmt"

	. "gitlab.com/DekaFinalProject/schedulelib/schedule"
)

type task struct {
	id     Identifier
	name   string
	effort Time

	requirements map[Identifier]ResourceAmount
}

func newTask(id Identifier) Task {
	return &task{
		id:     id,
		name:   "",
		effort: 0,

		requirements: make(map[Identifier]ResourceAmount),
	}
}

// SetID Allows to set ID when loading graph
func (t *task) SetID(id Identifier) {
	t.id = id
}

// ID returns internal ID for task.
func (t task) ID() Identifier {
	return t.id
}

// SetName sets displayed name for task.
func (t *task) SetName(name string) {
	t.name = name
}

// Name returns displayed name for task.
func (t task) Name() string {
	return t.name
}

// SetEffort sets effort in order to complete task.
func (t *task) SetEffort(time Time) {
	t.effort = time
}

// Effort returns effort in order to complete task.
func (t task) Effort() Time {
	return t.effort
}

func (t *task) SetResourceRequirement(idResource Identifier, amount ResourceAmount) {
	t.requirements[idResource] = amount
}

func (t *task) AcquireResources(collection ResourceCollection, time, duration Time) (Time, error) {
	var targetTimeAcquiring = time

	var targetTimeChanged = true

	for targetTimeChanged {
		targetTimeChanged = false

		for idResource, requestedAmount := range t.requirements {
			var rawRes = collection.Get(idResource)
			if rawRes == nil {
				return targetTimeAcquiring, fmt.Errorf("cannot find resource with ID '%d'", idResource)
			}
			var resourceInput, ok = rawRes.(*resource)
			if !ok {
				panic("simple.task: Library simple can be used only with resources from package simple")
			}

			if resourceInput != nil {
				var t, err = resourceInput.checkAcquire(requestedAmount, targetTimeAcquiring, duration)
				if err != nil {
					return t, err
				}

				if targetTimeAcquiring < t {
					targetTimeAcquiring = t
					targetTimeChanged = true
					break // Rebuild solution
				}
			}
		}
	}

	// Now grab resources from real collection
	for idResource, requestedAmount := range t.requirements {
		var resourceInput = collection.Get(idResource)
		if resourceInput != nil {
			var t, err = resourceInput.Acquire(requestedAmount, targetTimeAcquiring, duration)
			if err != nil {
				return t, err
			}
		}
	}

	return targetTimeAcquiring, nil
}
