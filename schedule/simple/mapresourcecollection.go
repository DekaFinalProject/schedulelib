package simple

import . "gitlab.com/DekaFinalProject/schedulelib/schedule"

// MapResourceCollection resource collection based on map type
type mapResourceCollection map[Identifier]Resource

func NewMapResourceCollection() ResourceCollection {
	return mapResourceCollection(make(map[Identifier]Resource))
}

func (c mapResourceCollection) Insert(r Resource) {
	c[r.ID()] = r
}

func (c mapResourceCollection) Get(id Identifier) Resource {
	var res, ok = c[id]
	if ok {
		return res
	}
	return nil
}

func (c mapResourceCollection) ResourceIDs() []Identifier {
	var ids = make([]Identifier, 0, len(c))
	for id := range c {
		ids = append(ids, id)
	}
	return ids
}

func (c mapResourceCollection) Len() int {
	return len(c)
}

func (c mapResourceCollection) Copy() Copyable {
	var copy = make(map[Identifier]Resource)
	for k, v := range c {
		copy[k] = v.Copy().(Resource)
	}
	return mapResourceCollection(copy)
}
