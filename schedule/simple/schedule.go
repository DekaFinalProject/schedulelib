package simple

import . "gitlab.com/DekaFinalProject/schedulelib/schedule"

type schedule struct {
	tasks map[Identifier]Task
	times map[Identifier]Time

	duration Time
}

func newSchedule() Schedule {
	return &schedule{
		tasks:    make(map[Identifier]Task),
		times:    make(map[Identifier]Time),
		duration: 0,
	}
}

func (s *schedule) AddTasks(tasks ...Task) {
	for _, t := range tasks {
		s.tasks[t.ID()] = t
	}
}

func (s *schedule) SetTaskStartTime(id Identifier, time Time) {
	s.times[id] = time

	// Update duration
	var task = s.tasks[id]
	var endTime = time + task.Effort()
	if s.duration < endTime {
		s.duration = endTime
	}
}

func (s schedule) TaskStartTime(id Identifier) (Time, bool) {
	time, ok := s.times[id]
	return time, ok
}

func (s schedule) Task(id Identifier) Task {
	t, ok := s.tasks[id]
	if !ok {
		return nil
	}
	return t
}

func (s schedule) TaskIDs() []Identifier {
	var tasksIds = make([]Identifier, 0, len(s.tasks))
	for tID := range s.tasks {
		tasksIds = append(tasksIds, tID)
	}

	return tasksIds
}

func (s schedule) IsTaskAssigned(id Identifier) bool {
	_, ok := s.times[id]
	return ok
}

func (s schedule) FullDuration() Time {
	return s.duration
}
