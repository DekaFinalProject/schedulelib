package simple

import (
	"testing"

	"github.com/stretchr/testify/assert"
	s "gitlab.com/DekaFinalProject/schedulelib/schedule"
)

func createTestGraph() s.AOEGraphModifiable {
	// Graphviz file for generated graph in /testdata/graph.dot
	var out = newAOEGraph()

	for i := 0; i < 5; i++ {
		out.SetEvent(newEvent(s.Identifier(i)))
	}

	for i := 1; i <= 6; i++ {
		out.AddTasks(newTask(s.Identifier(i)))
	}

	out.AddTaskEdge(1, 0, 3)
	out.AddTaskEdge(2, 3, 1)
	out.AddTaskEdge(3, 3, 1)
	out.AddTaskEdge(4, 1, 2)
	out.AddTaskEdge(5, 0, 2)
	out.AddTaskEdge(6, 2, 4)

	return out
}

func TestAOEGraphStartEventCalculation(t *testing.T) {
	var g = createTestGraph()
	var startE = g.StartEvent()
	assert.EqualValues(t, 0, startE.ID())
}

func TestAOEGraphEndEventCalculation(t *testing.T) {
	var g = createTestGraph()
	var endE = g.EndEvent()
	assert.EqualValues(t, 4, endE.ID())
}

func TestAOEGraphFromTest(t *testing.T) {
	var g = createTestGraph()

	// End point must not have from edges
	var edges = g.TasksFrom(4)
	assert.EqualValues(t, 0, len(edges))

	// Point with 2 edges
	edges = g.TasksFrom(0)
	assert.EqualValues(t, 2, len(edges))
	for _, e := range edges {
		if e.Task().ID() != 1 && e.Task().ID() != 5 {
			t.Error("unexpected edges from 0 event")
		}
	}

	// Point with 1 edges
	edges = g.TasksFrom(2)
	assert.EqualValues(t, 1, len(edges))
	for _, e := range edges {
		if e.Task().ID() != 6 {
			t.Error("unexpected edges from 2 event")
		}
	}
}
