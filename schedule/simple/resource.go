package simple

import (
	"fmt"
	"sort"

	. "gitlab.com/DekaFinalProject/schedulelib/schedule"
)

type resource struct {
	id         Identifier
	name       string
	consumable bool

	timeDiagram *timeDiagram
}

func newResource(id Identifier) Resource {
	return &resource{
		id:          id,
		name:        "",
		consumable:  true,
		timeDiagram: newTimeDiagram(),
	}
}

// SetID Allows to set ID when loading graph
func (r *resource) SetID(id Identifier) {
	r.id = id
}

// ID returns internal ID of resource.
func (r resource) ID() Identifier {
	return r.id
}

// SetName sets displayed name of resource.
func (r *resource) SetName(name string) {
	r.name = name
}

// Name returns displayed name of resource.
func (r resource) Name() string {
	return r.name
}

// SetConsumable sets if resource is consumable
func (r *resource) SetConsumable(isConsumable bool) {
	r.consumable = isConsumable
}

// Consumable returns if resource is consumable
func (r resource) Consumable() bool {
	return r.consumable
}

// Amount returns amount of resource at specified time.
func (r resource) Amount(time Time) ResourceAmount {
	return r.timeDiagram.Amount(time)
}

// Add adds amount of resources at specific time.
func (r *resource) Add(amount ResourceAmount, time Time) {
	r.timeDiagram.Add(amount, time)
}

// Subtract removes amount of resources at specific time.
func (r *resource) Subtract(amount ResourceAmount, time Time) {
	r.timeDiagram.Subtract(amount, time)
}

func (r *resource) AvailableAmount(time Time) (ResourceAmount, Time) {
	return r.timeDiagram.AvailableAmount(time)
}

func (r *resource) MinAmountInInterval(timeStart, duration Time) (ResourceAmount, Time) {
	return r.timeDiagram.MinAmountInInterval(timeStart, duration)
}

// Acquire tries to subtract required amount of resources
// as fast as possible. Returns time when resources captured.
// Error will be generated in case of resource will never be available .
//
// If resource is not consumable it can also release resource after specified duration
func (r *resource) Acquire(amount ResourceAmount, time Time, duration Time) (Time, error) {
	t, err := r.checkAcquire(amount, time, duration)
	if err != nil {
		return t, err
	}

	// Now t is where we can grab resources
	r.Subtract(amount, t)
	if !r.consumable {
		r.Add(amount, time+duration)
	}
	return t, nil
}

// checkAcquire tries to find point where resource can be acquired
// as fast as possible. Returns time when resources captured.
// Error will be generated in case of resource will never be available .
//
// If resource is not consumable it can also release resource after specified duration
func (r resource) checkAcquire(amount ResourceAmount, time Time, duration Time) (Time, error) {
	var a ResourceAmount // Available
	var t Time           // Crit Time point

	if r.consumable {
		a, t = r.timeDiagram.AvailableAmount(time)
	} else {
		a, t = r.timeDiagram.MinAmountInInterval(time, duration)
	}

	if amount <= a {
		if r.consumable {
			return time, nil
		}
		return t, nil
	}

	// Find time in changeTimes
	var timeIndex = sort.Search(len(r.timeDiagram.changeTimes), func(i int) bool {
		return r.timeDiagram.changeTimes[i] >= t
	})

	if timeIndex < len(r.timeDiagram.changeTimes)-1 {
		return r.checkAcquire(amount, r.timeDiagram.changeTimes[timeIndex+1], duration)
	}

	// We are on end of interval
	t = r.timeDiagram.changeTimes[len(r.timeDiagram.changeTimes)-1]
	a = r.Amount(t)
	if amount <= a {
		return t, nil
	}

	return t, fmt.Errorf("cannot acquire resource with ID '%d': too much requested", r.ID())
}

// Copy creates copy of resource descriptor
func (r resource) Copy() Copyable {
	return &resource{
		id:          r.id,
		name:        r.name,
		consumable:  r.consumable,
		timeDiagram: r.timeDiagram.Copy(),
	}
}
