package simple

import . "gitlab.com/DekaFinalProject/schedulelib/schedule"

type factory struct{}

func NewFactory() Factory {
	return &factory{}
}

func (f factory) NewResource(id Identifier) Resource {
	return newResource(id)
}

func (f factory) NewTask(id Identifier) Task {
	return newTask(id)
}

func (f factory) NewSchedule() Schedule {
	return newSchedule()
}

func (f factory) NewGraph() AOEGraphModifiable {
	return newAOEGraph()
}
