package simple

import (
	"testing"

	. "gitlab.com/DekaFinalProject/schedulelib/schedule"

	"github.com/stretchr/testify/assert"
)

func TestTimeDiagramInitialZero(t *testing.T) {
	var td = newTimeDiagram()

	assert.EqualValues(t, 0, td.Amount(0), "Expected zero value at time 0 with empty diagram")
	assert.EqualValues(t, 0, td.Amount(10), "Expected zero value at time 10 with empty diagram")
}

func TestTimeDiagramAdd(t *testing.T) {
	var td = newTimeDiagram()

	td.Add(10, 10)

	for i := 0; i < 20; i++ {
		if i < 10 {
			assert.EqualValues(t, 0, td.Amount(Time(i)), "Expected zero value before time 10")
		} else {
			assert.EqualValues(t, 10, td.Amount(Time(i)), "Expected 10 value t >=10")
		}
	}
}

func TestTimeDiagramSubtract(t *testing.T) {
	var td = newTimeDiagram()

	// 0  : t < 5
	// 10 : 5 <= t && t < 15
	// 5  : 15 <= t

	td.Add(10, 5)
	td.Subtract(5, 15)

	for i := 0; i < 20; i++ {
		if i < 5 {
			assert.EqualValues(t, 0, td.Amount(Time(i)), "Expected zero value before time 5")
		} else if 15 <= i {
			assert.EqualValues(t, 5, td.Amount(Time(i)), "Expected 5 value t >=15")
		} else {
			assert.EqualValues(t, 10, td.Amount(Time(i)), "Expected 10 value t = [5, 15)")
		}
	}
}

func TestTimeDiagramAvailableAmount(t *testing.T) {
	var td = newTimeDiagram()

	// 0  : t < 5
	// 10 : 5 <= t && t < 15
	// 5  : 15 <= t && t < 20
	// 15 : t <= 20

	td.Add(10, 5)
	td.Subtract(5, 15)
	td.Add(10, 20)

	var available, timepoint = td.AvailableAmount(0)
	assert.EqualValues(t, 0, available, "Expected zero available resource at time 0")
	assert.EqualValues(t, 0, timepoint, "Expected minimal value time at 0")

	available, timepoint = td.AvailableAmount(5)
	assert.EqualValues(t, 5, available, "Expected 5 available resources at time 5")
	assert.EqualValues(t, 15, timepoint, "Expected minimal value time at 15")

	available, timepoint = td.AvailableAmount(20)
	assert.EqualValues(t, 15, available, "Expected 15 available resources at time 20")
	assert.EqualValues(t, 20, timepoint, "Expected minimal value time at 20")

}

func TestTimeDiagramMinAmountInInterval(t *testing.T) {
	var td = newTimeDiagram()

	// 0  : t < 5
	// 10 : 5 <= t && t < 15
	// 5  : 15 <= t && t < 20
	// 15 : 20 <= t && t < 40
	// 0  : 40 <= t

	td.Add(10, 5)
	td.Subtract(5, 15)
	td.Add(10, 20)
	td.Subtract(15, 40)

	var available, timepoint = td.MinAmountInInterval(0, 20)
	assert.EqualValues(t, 0, available, "Expected zero available resource in [0, 20]")
	assert.EqualValues(t, 0, timepoint, "Expected minimal value time at 0")

	available, timepoint = td.MinAmountInInterval(5, 15)
	assert.EqualValues(t, 5, available, "Expected 5 available resources in [5, 20]")
	assert.EqualValues(t, 15, timepoint, "Expected minimal value time at 15")

	available, timepoint = td.MinAmountInInterval(20, 20)
	assert.EqualValues(t, 15, available, "Expected 5 available resources in [20, 40]")
	assert.EqualValues(t, 20, timepoint, "Expected minimal value time at 40")

}
