package simple

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNotConsumableAcquiring(t *testing.T) {
	var r = newResource(0)
	r.Add(10, 0)
	r.Subtract(5, 10)
	r.Add(5, 20)

	time, err := r.Acquire(10, 15, 5)
	assert.NoError(t, err)
	assert.EqualValues(t, 20, time)
}
