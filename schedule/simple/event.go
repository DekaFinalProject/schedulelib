package simple

import (
	. "gitlab.com/DekaFinalProject/schedulelib/schedule"
)

type event Identifier

func (e event) ID() Identifier {
	return Identifier(e)
}

func newEvent(id Identifier) Event {
	var e event = event(id)
	return &e
}
