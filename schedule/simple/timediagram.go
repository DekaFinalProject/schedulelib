package simple

import (
	"sort"

	. "gitlab.com/DekaFinalProject/schedulelib/schedule"
)

// timeDiagram holds amount of something in time
type timeDiagram struct {
	changeTimes    []Time
	resourceAmount map[Time]ResourceAmount
}

// newTimeDiagram creates new time diagram
func newTimeDiagram() *timeDiagram {
	return &timeDiagram{
		changeTimes:    make([]Time, 0, 10),
		resourceAmount: make(map[Time]ResourceAmount),
	}
}

// Amount returns amount at specified time
func (d timeDiagram) Amount(time Time) ResourceAmount {
	if len(d.changeTimes) == 0 {
		return 0
	}

	if time < d.changeTimes[0] {
		return 0
	}

	var i int
	for i = 1; i < len(d.changeTimes); i++ {
		if d.changeTimes[i-1] <= time && time < d.changeTimes[i] {
			break
		}
	}

	var key = d.changeTimes[i-1]
	return d.resourceAmount[key]
}

// Add adds amount at specified time
func (d *timeDiagram) Add(amount ResourceAmount, time Time) {
	d.insertTime(time)

	for i := 0; i < len(d.changeTimes); i++ {
		var t = d.changeTimes[i]
		if time <= t {
			d.resourceAmount[t] += amount
		}
	}
}

// Subtract subtracts amount at specified time
func (d *timeDiagram) Subtract(amount ResourceAmount, time Time) {
	d.insertTime(time)

	for i := 0; i < len(d.changeTimes); i++ {
		var t = d.changeTimes[i]
		if time <= t {
			d.resourceAmount[t] -= amount
		}
	}
}

// insertTime inserts timepoint if needed
func (d *timeDiagram) insertTime(time Time) {
	// Find time in changeTimes
	var timeIndex = sort.Search(len(d.changeTimes), func(i int) bool {
		return d.changeTimes[i] >= time
	})

	if timeIndex < len(d.changeTimes) && d.changeTimes[timeIndex] == time {
		// Time stamp have already exists
		return
	}

	// Insert time
	d.changeTimes = append(d.changeTimes, 0)                     // Increase capacity
	copy(d.changeTimes[timeIndex+1:], d.changeTimes[timeIndex:]) // Shift elements to right
	d.changeTimes[timeIndex] = time                              // Insert element

	// Insert resource amount
	if timeIndex == 0 {
		d.resourceAmount[time] = 0
	} else {
		var prevTime = d.changeTimes[timeIndex-1]
		d.resourceAmount[time] = d.resourceAmount[prevTime]
	}

}

// AvailableAmount calculates max available amount of resources from specified time.
// returns amount of resource that can be acquired and timepoint where restriction takes place
//
// If in future resource will be acquired by someone and resource amount will be lower than
// at specified time than available amount will be lower as well
func (d timeDiagram) AvailableAmount(time Time) (ResourceAmount, Time) {
	if len(d.changeTimes) == 0 {
		return 0, time
	}

	// Find time in changeTimes
	var timeIndex = sort.Search(len(d.changeTimes), func(i int) bool {
		return d.changeTimes[i] >= time
	})

	var availableAtTheTime ResourceAmount = 0

	if timeIndex < len(d.changeTimes) && d.changeTimes[timeIndex] == time {
		// Time stamp exists
		availableAtTheTime = d.resourceAmount[time]

	} else {
		// The time before any events
		if timeIndex == 0 && time < d.changeTimes[0] {
			return 0, time
		}

		// Previous time point specifies resource amount
		availableAtTheTime = d.resourceAmount[d.changeTimes[timeIndex-1]]
	}

	// Find minimal resource amount from 'time'
	var i int
	var minResourceAmount = availableAtTheTime
	var timeMinAmount = time
	for i = timeIndex; i < len(d.changeTimes); i++ {
		var timePoint = d.changeTimes[i]
		var available = d.resourceAmount[timePoint]

		if available < minResourceAmount {
			minResourceAmount = available
			timeMinAmount = timePoint
		}
	}

	return minResourceAmount, timeMinAmount
}

// MinAmountInInterval returns minimal value in specified interval [timestart; timestart+duration) and time when minimal will reached at first time
func (d timeDiagram) MinAmountInInterval(timeStart, duration Time) (ResourceAmount, Time) {
	if len(d.changeTimes) == 0 {
		return 0, timeStart
	}

	// Find time in changeTimes
	var timeIndex = sort.Search(len(d.changeTimes), func(i int) bool {
		return d.changeTimes[i] >= timeStart
	})

	var availableAtTheTime ResourceAmount = 0

	if timeIndex < len(d.changeTimes) && d.changeTimes[timeIndex] == timeStart {
		// Time stamp exists
		availableAtTheTime = d.resourceAmount[timeStart]

	} else {
		// The time before any events
		if timeIndex == 0 && timeStart < d.changeTimes[0] {
			return 0, timeStart
		}

		// Previous time point specifies resource amount
		availableAtTheTime = d.resourceAmount[d.changeTimes[timeIndex-1]]
	}

	// Find minimal resource amount from 'time'
	var i int
	var minResourceAmount = availableAtTheTime
	var timeMinAmount = timeStart
	for i = timeIndex; i < len(d.changeTimes); i++ {
		var timePoint = d.changeTimes[i]
		var available = d.resourceAmount[timePoint]
		if timeStart+duration <= timePoint {
			break
		}

		if available < minResourceAmount {
			minResourceAmount = available
			timeMinAmount = timePoint
		}
	}

	return minResourceAmount, timeMinAmount
}

// Copy creates new instance of diagram
func (d timeDiagram) Copy() *timeDiagram {
	out := &timeDiagram{
		changeTimes:    make([]Time, len(d.changeTimes)),
		resourceAmount: make(map[Time]ResourceAmount),
	}

	copy(out.changeTimes, d.changeTimes)

	for k, v := range d.resourceAmount {
		out.resourceAmount[k] = v
	}

	return out
}
