package simple

import (
	"errors"

	. "gitlab.com/DekaFinalProject/schedulelib/schedule"
)

type aoeGraphEdge struct {
	startEvent Event
	endEvent   Event
	task       Task
}

func newGraphEdge(s, e Event, t Task) TaskEdge {
	return &aoeGraphEdge{
		startEvent: s,
		endEvent:   e,
		task:       t,
	}
}

func (e aoeGraphEdge) StartEvent() Event {
	return e.startEvent
}

func (e aoeGraphEdge) EndEvent() Event {
	return e.endEvent
}

func (e aoeGraphEdge) Task() Task {
	return e.task
}

type simpleAOEGraph struct {
	tasks  map[Identifier]Task
	events map[Identifier]Event

	// mapping from ID of Event to ID of Event -> ID of Tasks
	mapping map[Identifier]map[Identifier][]Identifier

	startEvent Event
	endEvent   Event
}

func newAOEGraph() AOEGraphModifiable {
	return &simpleAOEGraph{
		tasks:   make(map[Identifier]Task),
		events:  make(map[Identifier]Event),
		mapping: make(map[Identifier]map[Identifier][]Identifier),

		startEvent: nil,
		endEvent:   nil,
	}
}

func (g *simpleAOEGraph) StartEvent() Event {
	if g.startEvent == nil {
		g.updateStartEvent()
	}
	return g.startEvent
}

func (g *simpleAOEGraph) EndEvent() Event {
	if g.endEvent == nil {
		g.updateEndEvent()
	}
	return g.endEvent
}

func (g simpleAOEGraph) TasksFrom(id Identifier) []TaskEdge {
	var edges = make([]TaskEdge, 0)

	subMapping, ok := g.mapping[id]
	if !ok {
		return edges
	}

	for target, taskIDs := range subMapping {
		for _, taskID := range taskIDs {
			var newEdge = newGraphEdge(newEvent(id), newEvent(target), g.tasks[taskID])
			edges = append(edges, newEdge)
		}
	}

	return edges
}

func (g simpleAOEGraph) TasksIDs() []Identifier {
	var tasks = make([]Identifier, 0, len(g.tasks))
	for id := range g.tasks {
		tasks = append(tasks, id)
	}
	return tasks
}

func (g simpleAOEGraph) Task(id Identifier) Task {
	t, ok := g.tasks[id]
	if !ok {
		return nil
	}
	return t
}

func (g simpleAOEGraph) EventsIDs() []Identifier {
	var events = make([]Identifier, 0, len(g.events))
	for id := range g.events {
		events = append(events, id)
	}
	return events
}

func (g simpleAOEGraph) Event(id Identifier) Event {
	e, ok := g.events[id]
	if !ok {
		return nil
	}
	return e
}

func (g *simpleAOEGraph) SetEvent(e Event) {
	g.events[e.ID()] = e
}

func (g *simpleAOEGraph) AddTasks(tasks ...Task) {
	for _, t := range tasks {
		g.tasks[t.ID()] = t
	}
}

func (g *simpleAOEGraph) AddTaskEdge(idTask, idStart, idEnd Identifier) error {
	task, ok := g.tasks[idTask]
	if !ok {
		return errors.New("no task with specified ID")
	}

	// Invalidate positions
	g.startEvent = nil
	g.endEvent = nil

	// Insure that all events exists
	beginEvent, ok := g.events[idStart]

	if !ok {
		beginEvent = newEvent(idStart)
		g.events[idStart] = beginEvent
	}

	endEvent, ok := g.events[idEnd]
	if !ok {
		endEvent = newEvent(idEnd)
		g.events[idEnd] = endEvent
	}

	// Insert to graph
	subMapping, ok := g.mapping[idStart]
	if !ok || subMapping == nil {
		g.mapping[idStart] = make(map[Identifier][]Identifier)
		subMapping = g.mapping[idStart]
	}

	slice, ok := subMapping[idEnd]
	if !ok || slice == nil {
		subMapping[idEnd] = make([]Identifier, 0, 1) // At least one element needed
		slice = subMapping[idEnd]
	}

	subMapping[idEnd] = append(slice, task.ID())

	return nil
}

func (g *simpleAOEGraph) updateStartEvent() {
	for idTest := range g.events {
		// Edges must only move from event.
		// Check if event not have edges entering to it
		var checkFailed = false
		for u := range g.mapping {
			for k := range g.mapping[u] {
				if k == idTest { // Edge enters into the event
					checkFailed = true
					break
				}
			}
			if checkFailed {
				break
			}
		}

		if !checkFailed { // If we here no such edges
			g.startEvent = g.Event(idTest)
			break
		}
	}
}

func (g *simpleAOEGraph) updateEndEvent() {
	// Edges must not exit from node
	for idTest := range g.events {
		var checkFailed = false
		for u := range g.mapping {
			if u == idTest {
				checkFailed = true
				break
			}
		}
		if !checkFailed {
			// If we here no such edges
			g.endEvent = g.Event(idTest)
			break
		}
	}
}
