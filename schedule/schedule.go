package schedule

type Schedule interface {
	// AddTasks adds task to schedule
	AddTasks(tasks ...Task)
	// SetTaskStartTime sets start time for specified task
	SetTaskStartTime(id Identifier, time Time)
	// TaskStartTime returns start time for a specified task the second parameter true if time getted
	TaskStartTime(id Identifier) (Time, bool)
	// Task returns task with specified id
	Task(id Identifier) Task

	// TaskIDs returns identifer of stored task
	TaskIDs() []Identifier
	// IsTaskAssigned returns true if task's start time have assigned via SetTaskStartTime
	IsTaskAssigned(id Identifier) bool

	// FullDuration returns duration of schedule
	FullDuration() Time
}
