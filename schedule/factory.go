package schedule

type Factory interface {
	// NewResource creates new resource with specified id
	NewResource(id Identifier) Resource
	// NewTask creates task with specified id
	NewTask(id Identifier) Task
	// NewSchedule creates new schedule
	NewSchedule() Schedule
	// NewGraph creates new modifiable graph
	NewGraph() AOEGraphModifiable
}
