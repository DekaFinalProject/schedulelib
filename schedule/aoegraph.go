package schedule

type TaskEdge interface {
	// StartEvent returns Event where task begins
	StartEvent() Event
	// EndEvent returns Event where task end
	EndEvent() Event
	// Task returns stored task
	Task() Task
}

type AOEGraph interface {
	// StartEvent returns event where no previous events
	StartEvent() Event
	// EndEvent returns event where no next events
	EndEvent() Event

	// TasksFrom returns all tasks edges that can be started from specified event
	TasksFrom(id Identifier) []TaskEdge

	// TasksIDs returns list of task identifiers
	TasksIDs() []Identifier
	// Task returns task in graph with specified id
	Task(id Identifier) Task

	// EventsIDs returns list of events identifiers
	EventsIDs() []Identifier
	// Event returns Event with specified id
	Event(id Identifier) Event
}

type AOEGraphBuilder interface {
	// SetEvent Add or replace event
	SetEvent(event Event)
	// AddTasks adds specified tasks without connections
	AddTasks(tasks ...Task)
	// AddTaskEdge adds connection in graph
	AddTaskEdge(idTask, idStart, idEnd Identifier) error
}

type AOEGraphModifiable interface {
	AOEGraph
	AOEGraphBuilder
}
