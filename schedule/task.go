package schedule

type Task interface {
	ResourceRequirements

	// SetID sets id
	SetID(id Identifier)
	// ID returns id
	ID() Identifier

	// SetName sets name
	SetName(name string)
	// Name returns name
	Name() string

	// SetEffort sets effort
	SetEffort(effort Time)
	// Effort returns effort for a task
	Effort() Time
}
