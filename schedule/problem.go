package schedule

type Problem interface {
	// Graph returns graph implementation of a problem
	Graph() AOEGraph
	// Resources returns resources of a problem
	Resources() ResourceCollection
}
