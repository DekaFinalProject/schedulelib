package schedule

type Event interface {
	// ID returns id of event
	ID() Identifier
}
