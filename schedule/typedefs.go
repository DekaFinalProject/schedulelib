package schedule

// Time timepoint in system
type Time uint32

// Identifier identifier
type Identifier int64

// ResourceAmount resource amount type
type ResourceAmount float64

// Copyable copyable objects
type Copyable interface {
	Copy() Copyable
}
