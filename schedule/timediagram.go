package schedule

type TimeDiagram interface {
	// Amount returns amount at specified timepoint
	Amount(time Time) ResourceAmount
	// Add adds specified amount at specified timepoint
	Add(amount ResourceAmount, time Time)
	// Subtract removes specified amount at specified timepoint
	Subtract(amount ResourceAmount, time Time)

	// AvailableAmount calculates max available amount of resources from specified time.
	// returns amount of resource that can be acquired and timepoint where restriction takes place
	//
	// If in future resource will be acquired by someone and resource amount will be lower than
	// at specified time than available amount will be lower as well
	AvailableAmount(time Time) (ResourceAmount, Time)

	// MinAmountInInterval returns minimal value in specified interval [timestart; timestart+duration) and time when minimal will reached at first time
	MinAmountInInterval(timeStart, duration Time) (ResourceAmount, Time)
}
