package scheduleunloader

import (
	"errors"
	"fmt"
	"io"

	"gitlab.com/DekaFinalProject/schedulelib/schedule"
)

type tsvUnloader struct {
	output io.Writer
}

func NewTSVUnloader(file io.Writer) Unloader {
	return &tsvUnloader{
		output: file,
	}
}

func (u *tsvUnloader) Unload(schedule schedule.Schedule) error {
	var ids = schedule.TaskIDs()
	for _, taskID := range ids {
		var task = schedule.Task(taskID)
		if task == nil {
			return errors.New("nil task detected during unloading")
		}

		startTime, ok := schedule.TaskStartTime(task.ID())
		if !ok {
			return errors.New("task with unassigned time detected")
		}

		_, err := fmt.Fprintf(u.output, "%d\t%s\t%d\t%d\n", task.ID(), task.Name(), startTime, startTime+task.Effort())
		if err != nil {
			return err
		}
	}

	return nil
}
