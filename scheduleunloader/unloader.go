package scheduleunloader

import (
	"io"

	"gitlab.com/DekaFinalProject/schedulelib/schedule"
)

type Unloader interface {
	Unload(schedule schedule.Schedule) error
}

type factoryFunctionType func(config interface{}) Unloader
type Factory struct {
	unloaders map[string]factoryFunctionType
}

func NewFactory() *Factory {
	var out Factory
	out.unloaders = make(map[string]factoryFunctionType)

	out.unloaders["tsv"] = func(config interface{}) Unloader {
		f, ok := config.(io.Writer)
		if !ok {
			return nil
		}
		return NewTSVUnloader(f)
	}

	return &out
}

func (f *Factory) NewUnloader(unloaderType string, config interface{}) Unloader {
	createFunc, ok := f.unloaders[unloaderType]
	if !ok {
		return nil
	}

	return createFunc(config)
}
